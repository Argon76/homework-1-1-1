let tabs = document.getElementsByClassName('tab');

let sections = document.getElementsByClassName('section');

for(let i =0; i<tabs.length; i++){
   tabs[i].onclick = tabclick
}

function tabclick(event){
  let tab = event.target;
  let tabId = tab.dataset.id;

  for(let k =0; k<tabs.length; k++){
    tabs[k].classList.remove('active');
    tabs[tabId-1].classList.add('active');

    sections[k].classList.remove('active');
    sections[tabId-1].classList.add('active'); 

  }
};

let response = [
  {
    img: './image/Layer 31.jpg'
  },
  {
    img: './image/Layer 28.jpg'
  },
  {
    img: './image/Layer 25.jpg'
  },
  {
    img: './image/Layer 29.jpg'
  },
  {
    img: './image/Layer 27.jpg'
  },
  {
    img: './image/_117310488_16.jpg'
  },
  {
    img: './image/Layer 33.jpg'
  },
  {
    img: './image/Layer 34.jpg'
  },
  {
    img: './image/Layer 24.jpg'
  },
  {
    img: './image/Layer 26.jpg'
  },
  {
    img: './image/Layer 30.jpg'
  },
  {
    img: './image/Layer 32.jpg'
  }

];
let addMoreBtn = document.getElementById('js-add-more-btn');
addMoreBtn.addEventListener('click', () => {
    addMoreBtn.innerText = 'loading...';


  
const container = document.getElementsByClassName('our-work__content')[0];
setTimeout(function() {
    const postlist = response.splice(0, 12);
    postlist.forEach(post => {
        const elPost = document.createElement('div');
        elPost.innerHTML = ` <img src="${post.img}" alt="#" class="content__img">
        <div class="content__hover">
            <div class="hover__content">
                <div class="link"><img src="./image/Combined shape 7431.png" alt=""></div>
                <i class="fas fa-stop-circle"></i>

            </div>
            <h3 class="design">creative design</h3>
            <h4 class="web">Web Design</h4>
        </div>`;
        container.append(elPost);
    });
    addMoreBtn.innerText = 'Add more';

    if(response.length === 0) {
        addMoreBtn.style.display = 'none';
    }
}, 2000);
})




function peopleSaySwitch(number) {
    let scrollFaces = document.querySelectorAll('.people_say_scroll_face');
    let scrollTexts = document.querySelectorAll('.people_say_text');
    scrollFaces.forEach(function(face){
        face.classList.remove('active');
        if(number == face.dataset.scrollFaceIndex) {
            face.classList.add('active');
        }
    });
    scrollTexts.forEach(function(text){
        text.classList.remove('active');
        if(number == text.dataset.scrollTextIndex) {
            text.classList.add('active'); 
        }
    });
}
let scrollButtons = document.querySelectorAll('.people_say_scroll_face');
scrollButtons.forEach(function(scrollButton){
    scrollButton.addEventListener('click', function(){
        peopleSaySwitch(this.dataset.scrollFaceIndex);
    });
});
let scrollArrows = document.querySelectorAll('.people_say_scroll .btn--scroll');
scrollArrows.forEach(function(scrollArrow){
    scrollArrow.addEventListener('click', function(){
        let position = document.querySelector('.people_say_scroll_face.active').dataset.scrollFaceIndex;
        if('left' == this.dataset.scrollDirection && 1 < position) {
            position--;
        }
        if('right' == this.dataset.scrollDirection && 4 > position) {
            position++;
        }
        peopleSaySwitch(position);
    });
});