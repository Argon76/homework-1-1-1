import logo from './logo.svg';
import React from 'react'
import './App.css';
import Button from './components/Button';
import Modal from './components/Modal';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      firstModal: false,
      secondModal: false,
      items: []
    }

  }

 
  componentDidMount() {
    fetch("../public/shop.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.items
          });
        })
      }



    modal1Open() {
      this.setState((state) => ({...state, firstModal: !state.firstModal}))
    }
    modal2Open(){
      this.setState((state) => ({...state, secondModal: !state.secondModal}))
    }
 
  
  render(){
    return (
    <div className="App">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}
  
}

export default App;
