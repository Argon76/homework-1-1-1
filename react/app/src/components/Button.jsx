import React from 'react'

class Button extends React.Component {

    constructor(props) {
        super(props)
        this.text = props.text
        this.backgroundColor = props.backgroundColor
        this.onClick = props.onClick
        this.onCancel = props.onCancel
    
}
    render() {
        return(
        <button onClick={this.props.onClick} className={this.props.backgroundColor}>{this.text}</button>
        )}
}

export default Button