import React from "react";
import './modal.scss'
import Button from "./Button.jsx";

class Modal extends React.Component {

 
    render() {
        return(
            <>
            {this.props.isOpen && 
       <div className="modal">
       <div className="modalBackDrop" onClick={this.props.onCancel}/>
       <div className="modalDialog">
           <div className="modalContent">
               <div className="modalHeader">
                   <h3 className="modalTitle">{this.props.header}</h3>
                   <Button text="x" className= {this.props.closeButton} onClick={this.props.onCancel}/>
               </div>
               <div className="modal-body">
                   <div className="ModalBody">
                       {this.props.text}
                    
                   </div>
               </div>
               <div className="ModalFooter">
                   <Button onClick={this.props.onCancel} className="btn secondaryButton" text='Cancel'/>
                   <Button className='btn primaryButton' text='Submit'/>
               </div>
           </div>
       </div>
   </div>}
</>)
    }
}

export default Modal