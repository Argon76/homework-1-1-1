import React from "react";
import './header.scss'

class Header extends React.Component {
    render() {
        return(
            <section class="header">
            <div class="logo">
              <img src="https://powderski.ru/800/600/https/pbs.twimg.com/media/DT-MU2QW4AE3hEO.jpg"></img>
            </div>
            <nav class="nav-list">
              <ul>
                <li><a href="#">HOME</a></li>
                <li><a href="#">FAVORITES</a></li>
                <li><a href="#">CART</a></li>
              </ul>
            </nav>
          </section>
        )
    }
}

export default Header;