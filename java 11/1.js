
let btn = document.querySelectorAll(".btn");

document.body.addEventListener("keypress", function (event) {

    btn.forEach((element) => {
        if (element.classList.contains("btn.active")) {
            element.classList.remove("btn.active");
        }
        if (element.innerText.toLowerCase() === event.key.toLowerCase()) {
            element.classList.add("btn.active");

        }
    })

});
