import React from "react";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";
import {useSelector} from "react-redux";
import Preloader from "../Preloader/Preloader";
const ItemsContainer = (props) => {
  const isLoading = useSelector(state => state.cards.isLoading)
  const { data } = props;

  return (
    <section className={styles.root}>
      <div className={styles.container}>
      {isLoading && <Preloader size={100} borderWidth={10}/>}
        {!isLoading &&
          data.map((elem) => (<Item key={elem.id}  id={elem.id} url={elem.url} price={elem.price} key={elem.id} name={elem.name}/>
          ))}
      </div>
    </section>
  );
};

export default ItemsContainer;
