import React from "react";
import styles from "./Header.module.scss";
import { Link } from "react-router-dom";

import { ReactComponent as NikeSVG } from "../../assets/shoe-sneaker.svg";

class Header extends React.Component {
  render() {
    return (
      <>
        <div className={styles.header}>
          <NikeSVG />
          <nav>
            <ul>
              <Link to="/">Home</Link>
              <Link to="/cart">Cart</Link>
              <Link to="/fav">Favorite</Link>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}

export default Header;
