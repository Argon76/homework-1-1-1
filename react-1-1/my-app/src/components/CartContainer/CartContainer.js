import React from 'react';
import Item from '../Item/Item';
import styles from './CartContainer.module.scss'
import {getItemFromLS} from '../../utils/localStorage'
import CartItem from '../../components/CartItem/CartItem'
import { useSelector } from 'react-redux';
import CartForm from '../CartForm/CartForm'

const CartContainer = () => {
  const cartData = useSelector((state)=>state.cart.cards)

  return (
    <section className={styles.root}>
     
        <div className={styles.container}>
        {cartData.map((elem)=><CartItem count={elem.count} name={elem.name} id={elem.id} img={elem.url} key={elem.id}  />)}
        </div>
        <CartForm/>
    </section>
)


// const cartDataParsed = JSON.parse(cartData)

}

export default CartContainer;