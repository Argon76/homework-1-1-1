import React from 'react'
import styles from './Button.module.scss'

class Button extends React.Component{



    
    render() {
const { title, handleClick, bgColor} = this.props

    return (
        <>
        <button className={styles.button}  onClick={handleClick}>{title}</button>
    </>)
}
}   
export default Button;