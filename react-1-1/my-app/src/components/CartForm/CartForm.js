import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import styles from "./CartForm.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import Button from "../Button/Button";
import { clearCartAC } from "../../store/actionCreators/cartAC";
import Preloader from "../Preloader/Preloader";
import {getItemFromLS, setItemToLS} from "../../utils/localStorage"
const CartForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    adress: "",
    phoneNumber: "",
  };
  const schema = yup.object().shape({
    firstName: yup.string().required('Поле обязательно'),
    lastName: yup.string().required('Поле обязательно'),
    age: yup.number('должно быть только число').max(99).positive().required('Поле обязательно'),
    adress: yup.string().required('Поле обязательно'),
    phoneNumber: yup.number().positive().required('Поле обязательно'),
  });

  const onSubmit = async (values, {resetForm}) => {
    try {
      const cartData = getItemFromLS('cart')
      console.log(cartData);
      console.log(values);
     dispatch(clearCartAC())
      resetForm();
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={schema}
    >
      <Form className={styles.form}>
        <div className={styles.inputCont}>
          <label>First Name</label>
          <Field name="firstName" type="name" className={styles.input} placeholder='First name'/>
          <ErrorMessage
            name="firstName"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Last Name</label>
          <Field name="lastName" type="name" className={styles.input}  placeholder='Last name'/>
          <ErrorMessage
            name="lastName"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Age</label>
          <Field name="age" type="age" className={styles.input}  placeholder='Age'/>
          <ErrorMessage name="age"  render={msg => <span className={styles.error}>{msg}</span>} />
        </div>
        <div className={styles.inputCont}>
          <label>Adress</label>
          <Field name="adress" type="adress" className={styles.input}  placeholder='Adress'/>
          <ErrorMessage
            name="adress"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Phone number</label>
          <Field
            name="phoneNumber"
            type="tel"
            className={styles.input}
            placeholder='Phone number'
          />
          <ErrorMessage
            name="phoneNumber"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div>
          <Button type="submit" title="Checkout"/>
        
        </div>
      </Form>
    </Formik>
  );
};
export default CartForm;
