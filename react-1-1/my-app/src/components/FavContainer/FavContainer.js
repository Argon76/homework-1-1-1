import React from 'react';
import styles from './FavContainer.module.scss'
import {getItemFromLS} from '../../utils/localStorage'
import FavItem from '../FavItem/FavItem'
import { useSelector } from 'react-redux';

const FavContainer = () => {
    const favData = useSelector((state)=>state.fav.cards)

    return (
        <section className={styles.root}>
         
            <div className={styles.container}>
            {!favData ? <p>There is no fav items</p> : favData.map((elem)=><FavItem count={elem.count} name={elem.name} id={elem.id} img={elem.url} key={elem.id} />)}
            </div>
        </section>
    )
}

export default FavContainer;