import React from "react";
import styles from "./Item.module.scss";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { ReactComponent as StarIcon } from "../../assets/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/star-remove.svg";
import {useDispatch} from "react-redux";
import {addCartItemAC} from "../../store/actionCreators/cartAC";
import {addFavItemAC} from "../../store/actionCreators/favAC"

const Item = (props) => {
  const { name, price, id, url } = props;
  const dispatch = useDispatch()

  return (
    <div className={styles.root}>
      <div className={styles.favourites} onClick={() =>{dispatch(addFavItemAC({name, url, id}))}}>
     <StarIcon />
      </div>
      <p>{name}</p>
      <img src={url} alt={name} />
      <span>{price}₴</span>
      <Button handleClick={()=> dispatch(addCartItemAC({name, url, id}))} title="Add to cart" />
    </div>
  );
};

export default Item;
