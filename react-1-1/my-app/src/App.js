import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./Routes/Routes";
import {getDataAC} from "./store/actionCreators/cardsAC";
import { useEffect, useState } from "react";
import {useDispatch} from "react-redux";

const App = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
        dispatch(getDataAC())
},[])

  return (
    <>
      <Header />
      <Modal />
      <AppRoutes
      />
      <Footer />
    </>
  );
};
export default App;
