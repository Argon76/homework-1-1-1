import React from 'react';

import {Routes, Route} from "react-router-dom";
import HomePage from '../pages/HomePage/HomePage';
import CartPage from '../pages/CartPage/CartPage';
import FavPage from '../pages/FavPage/FavPage'

const AppRoutes = (props) => {

    return (
        <Routes>
            <Route path='/' element={<HomePage />}/>
            <Route path='/cart' element={<CartPage />}/>
            <Route path='/fav' element={<FavPage />}/>
        </Routes>
    )
}
export default AppRoutes;