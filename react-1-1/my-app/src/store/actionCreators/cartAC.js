import {ADD_CART_ITEM, DELETE_CART_ITEM, PLUS_CART_ITEM, MINUS_CART_ITEM, CLEAR_CART} from "../actions/cartActions";

export const addCartItemAC = (data)=> {
    return {type: ADD_CART_ITEM, payload: data}
}
export const deleteCartItem = (id) =>{
    return {type: DELETE_CART_ITEM, payload: id}
}
export const plusCartItem = (id) =>{
    return {type: PLUS_CART_ITEM, payload: id}
}
export const minusCartItem = (id) =>{
    return {type: MINUS_CART_ITEM, payload: id}
}
export const clearCartAC = () =>( {type: CLEAR_CART })