async function getAdress() {
    let response = await fetch('https://api.ipify.org/?format=json');

    let getIp = await response.json();

    let adress = await fetch(`http://ip-api.com/json/${getIp.ip}?continent,country,countryCode,region,regionName,city,district&fields=continent,country,region,regionName,city,district`);

    let calculateAdress = await adress.json()
    console.log(calculateAdress)

    let { continent, country, region, city, district } = calculateAdress;

    const elAdress = document.getElementById("adress");

    elAdress.insertAdjacentHTML(
        "afterend",
        `<ul>
          <li>Continent: ${continent}<li>
          <li>Country: ${country}<li>
          <li>Region: ${region}<li>
          <li>City: ${city}<li>
          <li>District: ${district}<li>
          </ul>`
    );

}

const elButton = document.getElementById("button");

elButton.addEventListener('click', async (event) => { getAdress() });
