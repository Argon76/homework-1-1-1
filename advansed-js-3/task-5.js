// task 5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const BooksArr = [ ...books,bookAdd];
  
  console.log(BooksArr)