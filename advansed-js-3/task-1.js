// task 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let Arr = [...clients1, ...clients2];
let clientsSet = new Set(Arr);
Arr = [...clientsSet];
console.log(Arr);
